import {readFileSync} from 'jsonfile';
import {mkdirSync, writeFileSync} from 'fs';
import * as TOML from '@iarna/toml';

const DATADIR = process.argv[3];
const INPUTFILE = process.argv[2];

const origData = readFileSync(INPUTFILE);

const possibleProperties = new Set();

for (const run of origData) {
  const platform = run.platform;
  mkdirSync(`${DATADIR}/${platform}`, {
    recursive: true
  });
  Object.keys(run).forEach((property) => {possibleProperties.add(property);});
  let abbreviatedRunName = `${run.game ?? run.demo}-${run.category ? run.category: ""}-${run.time ? run.time: ""}`.replace(/\W/g, '');
  writeFileSync(`${DATADIR}/${platform}/${abbreviatedRunName}.toml`, TOML.stringify(run));
}
console.log(possibleProperties);


