import {writeFileSync} from 'jsonfile';
import {readFile, readdir, access} from 'fs/promises';
import * as TOML from '@iarna/toml';
import { existsSync, mkdirSync } from 'fs';
import { dirname } from 'path';

const MAX_DEPTH = parseInt(process.env.MAX_DEPTH) ?? 10;

type Run = {
  platform: string,
  game: string,
  category: string,
  youtubeID: string,
  time: string,
  authorList: string,
  publicationID: string,
  obsoleted: boolean,
  verified: boolean,
  verifiers: string,
  notes: string
}

const PropertyMap = new Map([
  ["game", "string"],
  ["category", "string"],
  ["youtubeID", "string"],
  ["time", "string"],
  ["authorList", "string"],
  ["publicationID", "string"],
  ["obsoleted", "boolean"],
  ["verified", "boolean"],
  ["verifiers", "string"],
  ["notes", "string"],
  ["exportedMovieID", "string"],
  ["demo", "string"],
  ["platform", "string"]
]);
const RequiredProperties = ["youtubeID", "authorList", "verified", "verifiers", "notes"];
const AllValidProperties = ["platform", "game", "category", "youtubeID", "time", "authorList", "publicationID", "obsoleted", "verified", "verifiers", "notes", "exportedMovieID", "demo"];

function loadData(basedir: string): Promise<Array<Run>> {
  console.log(`Processing Folder: ${basedir}`);
  return new Promise((resolve) => {
    readdir(basedir, { encoding: 'utf8', withFileTypes: true }).then((dirents) => {
      const promises = [];
      for (const dirent of dirents) {
        if (dirent.isDirectory()) {
          promises.push(loadRuns(`${basedir}/${dirent.name}`));
        }
      }
      Promise.all(promises).then((runs) => {
        resolve(runs.flat());
      });
    });
  });
}

function loadRuns(path: string, depth: number = 1): Promise<Array<Run>> {
  console.log(`Processing Folder: ${path}`);
  if (depth > MAX_DEPTH) return Promise.resolve([]);
  return new Promise((resolve) => {
    readdir(path, { encoding: 'utf8', withFileTypes: true }).then((dirents) => {
      const promises = [];
      for (const dirent of dirents) {
        if (dirent.isDirectory()) {
          promises.push(loadRuns(`${path}/${dirent.name}`, depth + 1));
          continue;
        }
        if (dirent.isFile()) {
          promises.push(loadRun(`${path}/${dirent.name}`));
          continue;
        }
      }
      Promise.all(promises).then((runs) => {
        resolve(runs.flat());
      });
    });
  });
}

function loadRun(file: string): Promise<Run> {
  console.log(`Processing File: ${file}`);
  return new Promise((resolve, reject) => {
    readFile(file, { encoding: "utf8"}).then((rawdata) => {
      const data = TOML.parse(rawdata);
      for (const property of RequiredProperties) {
        if (data[property] === undefined) {
          return reject(`Missing Property ${property} in ${file}`);
        };
      }
      if (data["demo"] === undefined && data["game"] === undefined) {
        return reject(`Missing Property game And demo in ${file}`);
      }
      for (const property of Object.keys(data)) {
        if (!AllValidProperties.includes(property)) {
          return reject(`Invalid Property ${property} in ${file}`);
        }
        if (typeof data[property] !== PropertyMap.get(property)) {
          return reject(`Wrong Type for Property ${property} in ${file}`);
        };
      }
      resolve({...data as Run });
    });
  });
}

const DATADIR = process.argv[2];
const OUTPUTFILE = process.argv[3];

access(DATADIR).then(() => {
  loadData(DATADIR).then((data) => {
    const OUTPUTDIR = dirname(OUTPUTFILE);
    if (!existsSync(OUTPUTDIR)){
      mkdirSync(OUTPUTDIR, { recursive: true });
    }
    writeFileSync(`${OUTPUTFILE}`, data);
  });
}).catch((err) => {
  console.error(err);
  process.exit(1);
});
