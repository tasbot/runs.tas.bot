var assert = require('assert');
var request = require("request");
var webdriver = require('selenium-webdriver');
const supportedCapabilities = {
  'device': 'iPhone 11',
  'realMobile': 'true',
  'os_version': '14.0',
  'browserName': 'iPhone',
  'name': 'Lazy Loading on Supported Devices', // test name
  'build': 'Runs.TAS.Bot Browserstack', // CI/CD job or build name
  'browserstack.user': process.env.browserstackUser,
  'browserstack.key': process.env.browserstackKey
}
const unsupportedCapabilities = {
  'device': 'iPhone SE',
  'realMobile': 'true',
  'os_version': '11',
  'browserName': 'iPhone',
  'name': 'Lazy Loading on Unsupported Devices', // test name
  'build': 'Runs.TAS.Bot Browserstack', // CI/CD job or build name
  'browserstack.user': process.env.browserstackUser,
  'browserstack.key': process.env.browserstackKey
}
function logTestResultInBrowserStack(result, sessionID) {
  request({ "uri": `https://${process.env.browserstackUser}:${process.env.browserstackKey}@api.browserstack.com/automate/sessions/${sessionID}.json`,
    "method": "PUT",
    "form": { "status": result ? "passed" : "failed" }
  });
}
describe('#SupportedBrowsersStartUnloaded', function () {
  this.timeout(30000);
  it('Should load the page without loading iframes marked for lazy loading', async function () {
    var driver = new webdriver.Builder().usingServer('https://hub-cloud.browserstack.com/wd/hub').withCapabilities(supportedCapabilities).build();
    await driver.get('https://runs.tas.bot/runs.html');
    var sessionData = await driver.session_;

    var srcAttributeMissing = !(await driver.findElement(webdriver.By.css('iframe.lozad')).getAttribute('src'));

    assert(srcAttributeMissing, "lazy element has a src attribute too soon");
    logTestResultInBrowserStack(srcAttributeMissing, sessionData.id_);
    driver.quit();
  });
});
describe('#UnsupportedBrowsersStartLoaded', function () {
  this.timeout(40000);
  it('Should fallback when loading the page on unsupported browsers', async function () {
    var driver = new webdriver.Builder().usingServer('https://hub-cloud.browserstack.com/wd/hub').withCapabilities(unsupportedCapabilities).build();
    await driver.get('https://runs.tas.bot/runs.html');
    var sessionData = await driver.session_;

    await driver.findElement(webdriver.By.css('button[aria-controls="Atari 2600"]')).click();
    await driver.sleep(3000);
    var linkElements = await driver.findElements(webdriver.By.partialLinkText("Watch on YouTube"));
    await driver.executeScript("arguments[0].scrollIntoView()", linkElements[0]);
    var fallbackWorked = !!linkElements.length;

    assert(fallbackWorked, "Lazy loading fallback failed to run");
    logTestResultInBrowserStack(fallbackWorked, sessionData.id_);
    driver.quit();
  });
});
describe('#SupportedBrowserLoads', function () {
  this.timeout(35000);
  it('Should load lazy iframes when they come into view', async function () {
    var driver = new webdriver.Builder().usingServer('https://hub-cloud.browserstack.com/wd/hub').withCapabilities(supportedCapabilities).build();
    await driver.get('https://runs.tas.bot/runs.html');
    var sessionData = await driver.session_;

    await driver.findElement(webdriver.By.css('button[aria-controls="Atari 2600"]')).click();
    await driver.sleep(3000);
    var lazyElementSrcAttribute = await driver.findElement(webdriver.By.css('iframe.lozad')).getAttribute("src");
    var srcAttributeExists = !!lazyElementSrcAttribute;

    assert(srcAttributeExists, "lazy element has a src attribute too soon");
    logTestResultInBrowserStack(srcAttributeExists, sessionData.id_);
    driver.quit();
  });
});