const gulp = require('gulp');
const del = require('del');
const download = require('gulp-download2');
const sourcemaps = require('gulp-sourcemaps');

const pug = require('gulp-pug');
const htmlmin = require('gulp-htmlmin');

const data = require('gulp-data');

const sass = require('gulp-dart-sass');
const csso = require('gulp-csso');
const postcss = require('gulp-postcss');
const cssmodules = require('postcss-modules');
const bootstrapUrl = "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css";

const terser = require('gulp-terser-js');

gulp.task('pages', function () {
    const bootstrapModule = require('./src/css/modules/bootstrap.min.css.json');
    const bootstrap = Object.assign((properties) => properties.map(function (property) {
        if (bootstrapModule.hasOwnProperty(property)) {
            return bootstrapModule[property];
        } else {
            console.log(`${property} is not a bootstrap class`);
        }
    }), bootstrapModule);

    return gulp.src('src/html/pages/**/*.pug')
        .pipe(data(function (file) {
            let filePath = file.history[0].replace(file.base, '').split('.')[0].substr(1);
        
            return {
                require: require,
                pageName: filePath,
                bootstrap: bootstrap,
                bootstrapUrl: bootstrapUrl
            };
        }))
        .pipe(pug({}))
        .pipe(htmlmin({
            collapseWhitespace: true,
            collapseBooleanAttributes: true,
            minifyCSS: true,
            minifyJS: true,
            removeComments: true,
            sortAttributes: true,
            sortClassName: true
        }))
        .pipe(gulp.dest('public')
    );
});

gulp.task('compileStyles', function () {
    return gulp.src('src/css/*.scss')
        .pipe(sourcemaps.init())
        .pipe(postcss(
            [cssmodules(
                {scopeBehaviour: "global",
                exportGlobals: true
            })]
        ))
        .pipe(sass({
            outputStyle: 'compressed',
            precision: 10,
            includePaths: ['.'],
            onError: console.error.bind(console, 'Sass error:')
        }))
        .pipe(csso())
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('public/css'));
});

function promisifyStream(stream) {
    return new Promise( res => stream.on('end',res));
}

gulp.task('download', async function () {
    await promisifyStream(download(bootstrapUrl)
        .pipe(gulp.dest('src/css/modules'))
    );
});

gulp.task('globalStyleExport', function () {
    return gulp.src('src/css/modules/*.css')
        .pipe(postcss(
            [cssmodules(
                {scopeBehaviour: "global",
                exportGlobals: true,
                localsConvention: 'camelCaseOnly'
            })]
        ));
});

gulp.task('styles', gulp.series('download', 'compileStyles', 'globalStyleExport'));

gulp.task('scripts', function () {
    return gulp.src('src/js/*.js')
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(terser({
        }))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('public/js'));
});

gulp.task('favicons', function () {
    return gulp.src('src/favicons/*')
    .pipe(gulp.dest('public/'));
});

gulp.task('movies', function () {
    return gulp.src('src/movies/*.zip')
    .pipe(gulp.dest('public/movies/'));
})

gulp.task('clean',  function () {
    return del(['public']);
});

gulp.task('default', gulp.series('styles', 'pages', 'scripts', 'favicons', 'movies'));

gulp.task('watch', function () {
    gulp.watch('src/html/**/*.pug', gulp.series('pages'));
    gulp.watch('src/data/*.json', gulp.series('pages'));
    gulp.watch('src/css/**/*.scss', gulp.series('styles', 'pages'));
    gulp.watch('src/js/**/*.js', gulp.series('scripts'));
});