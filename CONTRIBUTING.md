# RUNS.TAS.BOT

Inspired by local-minifier

https://github.com/amardeeprai/local-minifier

An archive of existing TASBot verifications using a custom Gulp Static Site Generator

## Development setup:

Install VS Code, the Live Server extension, git, and node current

Clone this repository

Run the following in the root directory
```
npm install
npm install --global gulp
npm run build
```

Now you should have a production ready "public" directory

### New Pages

Follow the example index.pug:

Extend the layout template with a new file in "src/html/pages" or a further subdirectory

.html files are generated under "public" matching the above directory structure

### New Scripts and Styles

SCSS files directly under src/css will generate CSS files in public/css

src/css/modules holds styles that are only meant to be included in other files

JavaScript works likewise, but without modules.  Modules can be done with browserify+webpack, but it forces bundling all JS in a single output file and make sourcemaps messy.

### Live Editing

Run "gulp watch" and open "public/index.html" with live server

Edits to the Pug, SASS, and JavaScript files in "src" are seen by "gulp watch" which triggers any relevant gulp tasks.  Live Server sees the resulting changes to the "public" directory and reloads the browser

See something in the output you don't like?  The gulp tasks generate sourcemaps for CSS and JS, so you can follow the chain back and see where a rule or script came from

### Hosting

In this GitLab implementation, CI/CD task runners take care of publishing updates to the master branch
