const allPlatforms = ["Atari 2600", "FDS", "GB", "GBA", "GBC", "Genesis", "Genesis 32X", "N64", "NES", "PC", "SNES"];
let platformToggles = document.querySelectorAll('.platformToggle');
let filter = {obsoleted: ["false","true"],
    verified: ["false","true"],
    platform: allPlatforms
};
let $table = $('#verifiedRunsList');

document.getElementById('showObsolete').addEventListener('change', function () {
    filter.obsoleted = this.checked ? ["false","true"] : ["false"];
    $table.bootstrapTable('filterBy', filter);
});

document.getElementById('showUnverified').addEventListener('change', function () {
    filter.verified = this.checked ? ["false","true"] : ["true"];
    $table.bootstrapTable('filterBy', filter);
});

document.getElementById('verifiedRunsList').addEventListener("click", (event) => {
    changeVideo(event);
});

document.getElementById('verifiedDemosList').addEventListener("click", (event) => {
    changeVideo(event);
});

document.querySelector('#platformToggleButtons').addEventListener("click", (event) => {
    platformToggles.forEach((platformToggle) => {
        if (platformToggle.contains(event.target)) {
            platformToggle.classList.toggle('active');
        }
    });
    let allPlatformsToggle = document.querySelector('.platformToggle[data-platform="all"');
    if (allPlatformsToggle.contains(event.target)) {
        if (allPlatformsToggle.classList.contains('active')) {
            platformToggles.forEach(platformToggle => platformToggle.classList.add('active'));
        } else {
            platformToggles.forEach(platformToggle => platformToggle.classList.remove('active'));
        }
    }
    filter.platform = Array.from(platformToggles)
        .filter((platformToggle) => platformToggle.classList.contains('active'))
        .map((platformToggle) => platformToggle.dataset.platform);
    $table.bootstrapTable('filterBy', filter);
});

// Load the IFrame Player API code asynchronously.
const youtubeScriptId = 'youtube-api';
let youtubeScript = document.getElementById(youtubeScriptId);
let player;

function changeVideo (event) {
    document.querySelectorAll('.watchYoutube').forEach((watchButton) => {
        if (watchButton.contains(event.target)) {
            document.getElementById('ytplayerWrapper').classList.remove('d-none');
            document.querySelectorAll(".table-active").forEach((activeRow) => {
                activeRow.classList.remove('table-active');
            });
            let newActiveRow = watchButton.parentNode.parentNode;
            let videoId = newActiveRow.dataset.youtubeid;
            newActiveRow.classList.add("table-active");
            if (youtubeScript === null) {
                let tag = document.createElement('script');
                let firstScript = document.getElementsByTagName('script')[0];
            
                tag.src = 'https://www.youtube.com/iframe_api';
                tag.id = youtubeScriptId;
                firstScript.parentNode.insertBefore(tag, firstScript);
            }
            
            if (player) {
                player.loadVideoById(videoId);
            } else {
                window.onYouTubeIframeAPIReady = () => {
                    player = new YT.Player('ytplayer', {
                        height: '360',
                        width: '640',
                        videoId: videoId
                    });
                }
            }
            document.getElementById('ytplayer').scrollIntoView();
        }
    });
}

function timeSorter(a, b) {
    if (hmsToSecondsOnly(a) < hmsToSecondsOnly(b)) return 1;
    if (hmsToSecondsOnly(b) < hmsToSecondsOnly(a)) return -1;
    return 0;
}

function hmsToSecondsOnly(str) {
    let p = str.split(':'),
        s = 0, m = 1;

    while (p.length > 0) {
        s += m * parseInt(p.pop(), 10);
        m *= 60;
    }

    return s;
}